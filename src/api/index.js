export default class Api {
    static api_key = process.env.REACT_APP_API_KEY;
    static baseUrl = 'https://api.themoviedb.org/3';
    static poster_url = 'http://image.tmdb.org/t/p/w342';

    static getGaners(){
      return new Promise(async(res,rej)=> {
          const response = await fetch(`${Api.baseUrl}/genre/movie/list?api_key=${this.api_key}&language=en-US`);
  if (response.status === 200){
     res(await response.json())
  }else rej (await response.json());     
        });
    }
 

    static getNowPlaying(page) {
      return new Promise(async(res,rej)=> {
          const response = await fetch(`${Api.baseUrl}/movie/now_playing?api_key=${this.api_key}&language=en-US&page=${page}`);
          if (response.status === 200){
            res(await response.json())
          }else rej (await response.json());     
        });
    }

static getDetaile(id){
    return new Promise(async(res,rej)=> {
        const response = await fetch(`${Api.baseUrl}/movie/${id}?api_key=${this.api_key}&language=en-US`);
if (response.status === 200){
   res(await response.json())
}else rej (await response.json());     
      });
  }
};