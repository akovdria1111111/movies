import store from "../store/store";

export function formatGanres(obj) {
    const {genres} = obj;
    const _genres = {};
    genres.forEach((el) => {
      _genres[el.id] = el.name;
     });
    return _genres;    
}

export function extractGanres( ganresList = []) {
const ganres = store.getState().ganres;
const result = ganresList.map((id) => ganres[id]);
return result.join(", ");
}