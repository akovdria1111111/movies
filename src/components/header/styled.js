import styled from "styled-components";
import {Link} from "react-router-dom";

export const Header = styled.header`
  background: linear-gradient(90deg, rgb(0, 0, 0), rgb(88, 82, 182) 90%);
  height: 70px; 
  padding: 15px; 
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding : 0 50px;
`;

const Flex = styled.div`
   align-items: center;
  justify-content: space-between;
`;

export const Nav = styled(Flex)`
width: 200px;
`;

export const NavLink = styled(Link)`
color: ${(props) => (props.is_active ? "#fff" : "#939da7")};
margin-right: 20px;
text-decoration: none;
transition: 0.2s linear all;
&:last-child {
  margin-right: 0;
}
&.active, 
&:hover {
  color: #fff;}
`;


