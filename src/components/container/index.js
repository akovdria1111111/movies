import styled from "styled-components";


export const Wrapper = styled.section`
width: 100vw;
height:calc(100vh - 70px);
overflow: auto;
padding: 30px 50px; 

`;

const Flex = styled.div`
display: flex;
`;

export const FlexCenter = styled(Flex)`
justify-content: center;
align-items: center;
`;

export const MovieList = styled(Flex)`
width: 100%;
flex-wrap: wrap;
justify-content: space-between;
`;