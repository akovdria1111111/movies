import moment from "moment";
import {useSelector, useDispatch} from "react-redux";

import {Card, CardHeader, Poster, ButtonLink, Colunm} from './styled';
import { ReactComponent as StarIcon} from "../../assets/star.svg";
import Api from '../../api';

import { extractGanres } from "../../helpers";
import {toggleFavorites} from "../../store/actions";

export const MovieItem = ({movie: {title, overview, poster_path, id, release_date, vote_average, genre_ids}}) => {
    const favorites = useSelector((s) => s.favorites);
    const dispatch = useDispatch();

    const onChangeFavorites = () => {
    dispatch(toggleFavorites(id));
    };

    return (<Card>
        <CardHeader isFavorite = {favorites.includes(id)}>
            <h3>{title}</h3>
            <StarIcon onClick = {onChangeFavorites}/>
        </CardHeader>
        <Poster src={Api.poster_url+poster_path} alt={title}/>
     <Colunm>
         <p>{overview}</p>
         <div className= "additional-info">Relise Data: {moment(release_date).format('DD MMM YYYY')}</div>
         <div className= "additional-info">Ganres: {extractGanres(genre_ids)}</div>
         <div className= "additional-info">Rating: {vote_average} </div>
        <ButtonLink to ={`/details/${id}`}>Details</ButtonLink>
        </Colunm>
      
    </Card> );
};