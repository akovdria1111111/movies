const initialState = {
    isAppReady: false,
    ganres: {},
    movies:{},
    moviesDetalies:{},
    totalPages: 0,
    favorites: JSON.parse(localStorage.getItem('favorites') || '[]'),

};

export const reducer = (state = initialState, action) => {
    if (action.type === "SET_READY") {
        return {...state, isAppReady: true};
    }
    if (action.type === "SET_GANRES") {
     return {...state, ganres: action.payload};
    }
    if (action.type === "SET_PAGE_MOVIES") {
     return {...state, movies: {...state.movies, [action.payload.page]: action.payload.results}, totalPages: action.payload.totalPages};
    }
    if (action.type === "SET_MOVIE_DETAILS") {
        return {...state, moviesDetalies: {...state.moviesDetalies, [action.payload.id]: action.payload.details } };
    }
    if (action.type === "SET_FAVORITES") {
        const _favorites = [...state.favorites];

        const index = _favorites.indexOf(action.payload);
    if (index !== -1) {
        _favorites.splice(index, 1);
    } else {
        _favorites.push(action.payload);
    }
    localStorage.setItem("favorites", JSON.stringify(_favorites));
    }
    return state;
};