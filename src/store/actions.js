export const onReady = () => ({type: "SET_READY"});

export const onSetGanres = (ganres) => ({type: "SET_GANRES", payload: ganres});

export const onSetPage =  (page, results, totalPages) => {
    return {type: "SET_PAGE_MOVIES", payload: {page, results, totalPages}};
};

export const onSetDetails =  (id, details) => ({type: "SET_MOVIE_DETAILS", payload: {id, details}});

export const toggleFavorites = (id) => ({type: "SET_FAVORITES", payload: id});