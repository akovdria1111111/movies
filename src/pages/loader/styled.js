import styled from "styled-components";

export const Loader = styled.div`
height: 250px;
width: 250px;
border-radius: 50%;
background: #000;
position: relative;
overflow: hidden;
border: 5px solid #1a1a1a;
display: flex;
justify-content: center;
align-content: center;
align-items: center;
top: 250px;
color: #fff;

left: 40%;

&::after {
    content: "";
    position: absolute;
    top: 25%;
    width: 200%;
    height: 200%;
    left: -50%;

    background: rgba (83, 252, 83, 0,5);
    box-shadow: 0 0 20px rgb(88, 82, 182);

    border-radius: 40%;
    animation: rotate 10s linear forwards infinite;
}

@keyframes rotate {
    to {
        transform: rotate(360deg);
    }
}

`;