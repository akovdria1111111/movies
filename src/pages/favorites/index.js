import { useState, useEffect} from "react";
import {useSelector} from "react-redux";

import Api from "../../api";
import { MovieItem } from '../../components/movieitem';
import {MovieList} from '../../components/container';
import { LoaderMovies } from '../loader';

export const Favorite = () => {
   const favorites = useSelector((s) => s.favorites);
   const [list, setList] = useState(null);

   useEffect(() => {
        Promise.all(favorites.map((id) => Api.getDetaile(id))).then((responses) => setList(responses));
   }, []);

   const renderItems = () => list.map((movie) => <MovieItem key={movie.id} movie={movie} />);

   if (!list)
    return <LoaderMovies/>

    return <MovieList>{renderItems()}</MovieList>;
};