import { useEffect } from 'react';
import {useParams, useHistory} from "react-router-dom";
import ReactPaginate from 'react-paginate';
import {useDispatch, useSelector} from "react-redux";

import Api from "../../api";
import { MovieItem } from '../../components/movieitem';
import {MovieList} from '../../components/container';

import {onSetPage} from "../../store/actions";
import { LoaderMovies } from '../loader';

export const Main = () => {
    const dispatch = useDispatch();
    const { movies, totalPages} = useSelector ((s) => s);
    const  { page } = useParams();
    const history = useHistory();

    useEffect(() => {
        if (!movies[page]) {
            Api.getNowPlaying(page).then(({results,total_pages}) => {
                dispatch(onSetPage(page, results, total_pages));
            });
        }
    }, [page]);

    const renderItems = () => movies[page].map((movie) => <MovieItem key={movie.id} movie={movie} />);

    const onPageChange = (event) => {
       history.push(`/movies/${event.selected + 1}`);
      };

    return (
    <>
        <ReactPaginate
        className="paginator"
        breakLabel="..."
        nextLabel=" >"
        onPageChange={onPageChange}
        pageRangeDisplayed={5}
        pageCount={totalPages}
        previousLabel="< "
        renderOnZeroPageCount={null}
      />
       
        <MovieList>{movies[page] ? renderItems() : <LoaderMovies/> }</MovieList>
    </>
    );
};
