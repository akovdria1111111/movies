import {useParams, useHistory} from "react-router-dom";
import { useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";

import Api from "../../api";
import {onSetDetails} from "../../store/actions";

import {CardHeader, ButtonBack, CardBox, Poster, Card} from './styled';
import { LoaderMovies } from '../loader';

export const Details = () => {

    const  {id} = useParams();
    const history = useHistory();
    const dispatch = useDispatch();
    const _details = useSelector((s) => s.moviesDetalies[id]);

    useEffect(() => {
        if (!_details){
        Api.getDetaile(id).then((r) => dispatch(onSetDetails(id, r)));
        }
    }, [id]);

    if(!_details) return <LoaderMovies/>

    return (
    <div>Details
        <CardHeader>
        <ButtonBack onClick={() => history.goBack()}>Back</ButtonBack> 
        <h3>{JSON.stringify(_details.title)}</h3>
        </CardHeader>
        <CardBox>
        <Poster src={Api.poster_url+_details.backdrop_path} alt="title"/>
        <Card>{JSON.stringify(_details.overview)}</Card>
        </CardBox>
    </div>);
};