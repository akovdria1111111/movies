// import {Link} from "react-router-dom";
import styled from "styled-components";

export const ButtonBack = styled.button`
display: inline-flex;
align-items: center;
justify-content: center;
border: none;
cursor: pointer;
padding: 14px 32px;
font-size: 12px;
background: rgb(88,82,182);
color: white;
text-align: center;
text-transform: uppercase;
font-weight: 500;
letter-spacing: 0.05en;
line-height: 12px;
border-radius: 3px;
height: 44px;
transition: all 0.3s cubic-bezier(0.165, 0.84, 0.44, 1);
width: 100px;
min-width: 100px;
text-decoration: none;
`;

export const CardHeader = styled.div`
background: linear-gradient(90deg, rgb(0, 0, 0), rgb(88, 82, 182) 90%);
width: 100%;
height: 50px;
padding: 15px;
display: flex;
align-items: center;
justify-content: space-between;
>h3 {
    margin: 0;
    color: #fff;
    font-size: 14px;
}
`;

export const CardBox = styled.div`
    width: 100%;
    height: 500px;
    margin-top: 15px;
    overflow: hidden;
    display: grid;
    grid-template-columns: 1fr 1fr;
    `;

export const Poster = styled.img`
width: 500px;
height: 350px;
object-fit: cover;
margin-bottom: 20px;
padding: 5px;
`;   
export const Card = styled.div`
    font-size: 12px;
    color: #939da9;
    height: 75px;
    padding: 5px;
`;