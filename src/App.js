import {Fragment, useEffect} from 'react';
import {Switch, Route, Redirect } from 'react-router-dom';
import {useSelector, useDispatch} from "react-redux";
import './App.css';

import {Main} from './pages/main';
import {Favorite} from './pages/favorites';
import {Details} from './pages/movieDetails';
import { notFound } from './pages/notFound';
import { LoaderMovies } from "./pages/loader";

import {Header} from './components/header';
import {Wrapper} from './components/container';
import Api from './api';

import {onReady, onSetGanres} from "./store/actions";
import {formatGanres} from "./helpers";

function App() {
  const dispatch = useDispatch ();
  const isAppReady = useSelector((store) => store.isAppReady);

  useEffect(() => {
    if (!localStorage.getItem('favorites')) localStorage.setItem('favorites', JSON.stringify([]));

    Api.getGaners().then((res) => {
      dispatch(onSetGanres(formatGanres(res)));
      dispatch(onReady());
    });
    // eslint-disable-next-line
  },[]);

   
  if (!isAppReady) 
    return (<LoaderMovies/>);
  return (
    <Fragment>
      <Header/>
     <Wrapper>
       <Switch>
    <Route exact path="/" component = {() => <Redirect to = "/movies/1" /> } />
    <Route path="/movies/:page" component={Main} />
    <Route path="/details/:id" component={Details}/>
    <Route path="/favorites" component={Favorite}/>
    <Route component={notFound}/>
       </Switch>
     </Wrapper>
    </Fragment>
  );
}

export default App;
